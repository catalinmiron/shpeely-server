/**
 * Main application routes
 */


import path from 'path';
import errors from './components/errors';
import thing from './api/thing';
import graphql from './api/graphql';
import bgg from './api/bgg';
import user from './api/user';
import auth from './auth';

export default function (app) {
  // Insert routes below
  app.use('/api/things', thing);
  app.use('/api/graphql', graphql);
  app.use('/api/users', user);
  app.use('/api/bgg', bgg);

  app.use('/api/auth', auth);

  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*')
   .get(errors[404]);

  // All other routes should redirect to the index.html
  app.route('/*')
    .get((req, res) => {
      res.sendFile(path.resolve(`${app.get('appPath')}/index.html`));
    });
}
