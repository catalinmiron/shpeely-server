/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */


import Thing from '../api/thing/thing.model';
import User from '../api/user/user.model';
import BggData from '../api/bgg/bgg.model';

Thing.find({}).remove()
  .then(() => {
    Thing.create({
      name: 'Development Tools',
      info: 'Integration with popular tools such as Webpack, Gulp, Babel, TypeScript, Karma, '
      + 'Mocha, ESLint, Node Inspector, Livereload, Protractor, Pug, '
      + 'Stylus, Sass, and Less.',
    }, {
      name: 'Server and Client integration',
      info: 'Built with a powerful and fun stack: MongoDB, Express, '
      + 'AngularJS, and Node.',
    }, {
      name: 'Smart Build System',
      info: 'Build system ignores `spec` files, allowing you to keep '
      + 'tests alongside code. Automatic injection of scripts and '
      + 'styles into your index.html',
    }, {
      name: 'Modular Structure',
      info: 'Best practice client and server structures allow for more '
      + 'code reusability and maximum scalability',
    }, {
      name: 'Optimized Build',
      info: 'Build process packs up your templates as a single JavaScript '
      + 'payload, minifies your scripts/css/images, and rewrites asset '
      + 'names for caching.',
    }, {
      name: 'Deployment Ready',
      info: 'Easily deploy your app to Heroku or Openshift with the heroku '
      + 'and openshift subgenerators',
    });
  });

User.find({}).remove()
  .then(() => {
    User.create({
      provider: 'local',
      name: 'Test User',
      email: 'test@example.com',
      password: 'test',
    }, {
      provider: 'local',
      role: 'admin',
      name: 'Admin',
      email: 'admin@example.com',
      password: 'admin',
    })
      .then(() => {
        console.log('finished populating users');
      });
  });


// const BggDataSchema = new mongoose.Schema({
//   bggid: Number,
//   name: String,
//   description: String,
//   weight: Number,
//   rating: Number,
//   maxPlayers: Number
// });

BggData.find({}).remove()
  .then(() => {
    BggData.create({
      bggid: 31260,
      name: 'Agricola',
      description: 'A game where you play a farmer and make vegetables and cows and stuff.',
      weight: 4.2,
      rating: 4.3,
      maxPlayers: 5,
      thumbnail: 'https://cf.geekdo-images.com/images/pic259085_md.jpg',
      rank: 15,
      ownedBy: 10000,
      playTime: 280,
      yearPublished: 2005,
    }, {
      bggid: 25613,
      name: 'Through the Ages',
      description: 'A game where you try to build a civilization with culture and at ' +
      'the same time crush your opponent with your army.',
      weight: 4.3,
      rating: 4.4,
      maxPlayers: 4,
      thumbnail: 'https://cf.geekdo-images.com/images/pic236169_md.jpg',
      rank: 5,
      ownedBy: 8000,
      playTime: 360,
      yearPublished: 2006,
    })
      .then(() => {
        console.log('finished populating users');
      });
  });

