/**
 * Socket.io configuration
 */
import logger from 'winston';
import thingSocket from '../api/thing/thing.socket';
import statsSocket from '../api/graphql/graphql.socket';
// import config from './environment';

// When the user disconnects.. perform this
function onDisconnect(/* socket*/) {}

// When the user connects.. perform this
function onConnect(socket) {
  // When the client emits 'info', this listens and executes
  socket.on('info', (data) => {
    socket.log(JSON.stringify(data, null, 2));
  });

  // Insert sockets below
  // thingSocket.register(socket);
  statsSocket.register(socket);
}

export default function (socketio) {
  // socket.io (v1.x.x) is powered by debug.
  // In order to see all the debug output, set DEBUG (in server/config/local.env.js) to including the desired scope.
  //
  // ex: DEBUG: "http*,socket.io:socket"

  // We can authenticate socket.io users and access their token through socket.decoded_token
  //
  // 1. You will need to send the token in `client/components/socket/socket.service.js`
  //
  // 2. Require authentication here:
  // socketio.use(require('socketio-jwt').authorize({
  //   secret: config.secrets.session,
  //   handshake: true
  // }));

  statsSocket.registerChannel(socketio);

  socketio.on('connection', (socket) => {
    logger.debug('Client connected: ', socket.id);
    /* eslint-disable no-param-reassign */
    socket.address = `${socket.request.connection.remoteAddress}:${socket.request.connection.remotePort}`;

    socket.connectedAt = new Date();

    socket.log = (...data) => {
      console.log(`SocketIO ${socket.nsp.name} [${socket.address}]`, ...data);
    };
    /* eslint-enable no-param-reassign */

    // Call onDisconnect.
    socket.on('disconnect', () => {
      onDisconnect(socket);
      socket.log('DISCONNECTED');
    });

    // Call onConnect.
    onConnect(socket);
    socket.log('CONNECTED');

    socket.emit('connection', {lol:123});

  });

}
