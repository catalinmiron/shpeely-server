import winston from 'winston';
import 'winston-loggly-bulk';
import config from './environment';

// Setup logger
winston.remove(winston.transports.Console);
winston.add(winston.transports.Console, Object.assign({}, config.logging));

if (config.loggly && config.loggly.token) {
  winston.add(winston.transports.Loggly, {
    token: config.loggly.token,
    subdomain: 'shpeely',
    tags: ['Shpeely Server', ...(config.loggly.tags || [])],
    json: true,
    ...config.logging
  });
  winston.debug('Added loggly winston transport.');
}

winston.debug('Configured logging...');

export default winston;
