function getArray(envVariable) {
  return envVariable
    ? envVariable.split(',')
    : null;
}

/* eslint no-process-env:0*/

// Production specific configuration
// =================================
module.exports = {
  // Should we run the startup tasks?
  runStartupTasks: true,

  // Should we run the cron jobs?
  runCronJobs: true,

  // loggly configuration
  loggly: {
    token: process.env.LOGGLY_TOKEN
  },

  secrets: {
    session: process.env.SESSION_SECRET,
  },

  // Server IP
  ip: process.env.OPENSHIFT_NODEJS_IP
    || process.env.ip
    || undefined,

  // Server port
  port: process.env.OPENSHIFT_NODEJS_PORT
    || process.env.PORT
    || 8080,

  // MongoDB connection options
  mongo: {
    uri: process.env.MONGODB_URI
      || process.env.MONGOHQ_URL
      || process.env.OPENSHIFT_MONGODB_DB_URL + process.env.OPENSHIFT_APP_NAME
      || 'mongodb://localhost/shpeelyserver',
  }
};
