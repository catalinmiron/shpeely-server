

/* eslint no-process-env:0*/

// Development specific configuration
// ==================================
module.exports = {

  // MongoDB connection options
  mongo: {
    uri: 'mongodb://127.0.0.1/shpeelyserver-dev',
  },

  gameStats: {
    uri: process.env.GAME_STATS_URI || 'http://localhost:5000'
  },

  graphCool: {
    uri: 'https://api.graph.cool/simple/v1/shpeely-dev',
    token: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MDgwNzgyODIsImNsaWVudElkIjoiY2ozang1ZHNzZXVvbTAxNzBubjcxbngzNCIsInByb2plY3RJZCI6ImNqOGFrczM0ejBmbnUwMTIzdXRubTNxamYiLCJwZXJtYW5lbnRBdXRoVG9rZW5JZCI6ImNqOHN1dmU1ajAyZnQwMTI5cXJna2FlaTUifQ.UU6r0TMSBlU-y80npev9JVpdC1s_Gx6DrGHmj6RMu0s'
  },

  // Seed database on startup
  seedDB: false,

};
