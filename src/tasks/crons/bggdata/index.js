// Cron job for syncing BGG data
import logger from 'winston';
import { getOldest, syncPlain } from '../../../api/bgg/bgg.controller';

/**
 * The cron interval string
 * @type {string}
 */
const interval = '* */30 * * * *';

/**
 * The periodic task
 * @returns {Promise}
 */
async function task() {
  // find oldest bgg document and sync it with bgg
  const oldest = await getOldest();
  if (oldest) {
    return syncPlain(oldest.bggid)
      .then(() => logger.debug('Updated bgg data of game %s', oldest.bggid))
      .catch(err => logger.error('Failed to sync game', err));
  }
  logger.debug('No BGG data was updated because database doesn\'t contain any BGG data.');
}

export default {
  interval, task, immediate: true
};
