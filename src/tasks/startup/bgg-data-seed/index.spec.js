const proxyquire = require('proxyquire').noPreserveCache();

const syncPlain = sinon.stub().returns(Promise.resolve({ name: 'fake game' }));

const bggData = proxyquire('./index.js', {
  '../../../api/bgg/bgg.controller': {
    syncPlain,
  }
}).default;

describe('BGG Data Cron:', () => {
  let clock;

  beforeEach(() => {
    clock = sinon.useFakeTimers();
  });

  afterEach(() => clock.restore());

  describe('Run task', () => {
    it('Should download the given BGG ids', async () => {
      // given
      const promise = bggData.start([1, 2, 3]);

      // when
      clock.tick(10e5);
      await promise;

      // then
      expect(syncPlain.withArgs(1)).to.have.been.calledOnce;
      expect(syncPlain.withArgs(2)).to.have.been.calledOnce;
      expect(syncPlain.withArgs(3)).to.have.been.calledOnce;
      expect(promise).to.be.fulfilled;
    });
  });

});
