// Startup job to synchronize GraphCool with the data on shpeely.com

import logger from 'winston';
import legacyShpeelyClient from '../../../services/legacy-shpeely';
import graphqlClient from '../../../services/graphql';
import { getBggInfoByBggId } from '../../../services/graphql';
import { syncPlain } from '../../../api/bgg/bgg.controller';

export const JOB_NAME = 'legacy-shpeely-sync';

function deleteNodes (schemaName) {
  return `
    mutation ($${schemaName.toLowerCase()}Id: ID!) {
      delete${schemaName} (
        id: $${schemaName.toLowerCase()}Id
      ) {
        id
      }
    }
  `;
}

function queryAll (schemaName) {
  return `
    query  {
      ${schemaName.toLowerCase()}: all${schemaName}${schemaName.endsWith('o') ? 'e' : ''}s {
        id
      }
    }
  `;
}

const createTournamentMutation = `
  mutation createTournament($name: String!, $slug: String!, $players: [TournamentplayersPlayer!]!) {
    tournament: createTournament(
      name: $name
      slug: $slug
      players: $players
    ) {
      id
      players {
        name
        id
      }
    }
  }
`;

const createGameresultMutation = `
  mutation (
    $tournamentId: ID!
    $bggInfoId: ID!
    $scores: [GameresultscoresScore!]!
    $time: DateTime!
    $numPlayers: Int!
  ) {
    gameresult: createGameresult (
      time: $time
      tournamentId: $tournamentId
      bggInfoId: $bggInfoId
      scores: $scores
      numPlayers: $numPlayers
    ) {
      id
    }
  }
`;




async function deleteSchema (schemaName) {
  const ids = await getAllIds(schemaName);
  await deleteAllIds(schemaName, ids);
}

async function getAllIds (schemaName) {
  return (await graphqlClient.request(queryAll(schemaName)))[schemaName.toLowerCase()]
    .map(t => t.id)
}

async function deleteAllIds (schemaName, ids) {
  for (let id of ids) {
    await graphqlClient.request(deleteNodes(schemaName), {
      [`${schemaName.toLowerCase()}Id`]: id
    });
  }
}

/**
 * The task to execute
 */
async function start() {

  // delete all existing data

  await deleteSchema('Score');
  logger.debug('deleted scores');
  await deleteSchema('Gameresult');
  logger.debug('deleted gameresults');
  await deleteSchema('Player');
  logger.debug('deleted players');
  await deleteSchema('Tournament');
  logger.debug('deleted tournaments');
  await deleteSchema('BggInfo');
  logger.debug('deleted tournaments');

  // create tournaments
  const tournamentsShpeely = await legacyShpeelyClient('/tournaments');
  for (let tournament of tournamentsShpeely.reverse()) {
    logger.debug('Create tournament ', tournament.name);

    const name = tournament.name;
    const slug = tournament.slug;
    const players = tournament.members.map((p) => ({
      name: p.name,
      role: p.role.toUpperCase()
    }));

    const graphqlTournament = await graphqlClient.request(createTournamentMutation, {
      name,
      slug,
      players
    });

    // create game results
    let gameresultsShpeely = await legacyShpeelyClient(`/tournaments/${tournament._id}/gameresults`);
    const cache = {};
    for (let gameresult of gameresultsShpeely) {
      logger.debug(`Migrate game result ${gameresult._id} of tournament ${tournament.name}`);

      const time = gameresult.time;
      const tournamentId = graphqlTournament.tournament.id;
      const bggid = gameresult.game.id;
      const numPlayers = gameresult.scores.length
      const scores = gameresult.scores.map((s) => ({
        score: Math.round(s.score),
        playerId: graphqlTournament.tournament.players.find(p => p.name === s.player.name).id
      }));

      // create bgginfo if it doesn't exist already
      let bggInfoId = cache[bggid] // check if cached
      if (!bggInfoId) {
        const graphQlBggInfo = await getBggInfoByBggId(bggid); // check if already in graphql
        if (!graphQlBggInfo) {
          let { id } = await syncPlain(bggid); // create in graphql
          bggInfoId = id;
        } else {
          bggInfoId = graphQlBggInfo.id;
        }
        cache[bggid] = bggInfoId;
      }

      await graphqlClient.request(createGameresultMutation, {
        tournamentId,
        bggInfoId,
        time,
        scores,
        numPlayers
      });

      logger.debug(`Migrated game ${gameresult._id}`);
    }
  }
}

export default {
  start,
  name: JOB_NAME
};
