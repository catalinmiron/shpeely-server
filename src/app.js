/**
 * Main application file
 */

import 'regenerator-runtime/runtime';
import express from 'express';
import http from 'http';
import mongoose from 'mongoose';

import logger from './config/logger';
import config from './config/environment';
import tasks from './tasks';

mongoose.Promise = require('bluebird');

// Connect to MongoDB
mongoose.connect(config.mongo.uri, config.mongo.options);
mongoose.connection.on('error', (err) => {
  logger.error(`MongoDB connection error: ${err}`);
  process.exit(-1); // eslint-disable-line no-process-exit
});
logger.info('Connected to ', config.mongo.uri);


// Populate databases with sample data
if (config.seedDB) {
  // eslint-disable-next-line global-require
  require('./config/seed');
}

// Setup server
const app = express();
const server = http.createServer(app);
const socketio = require('socket.io')(server, {
  serveClient: true
});
require('./config/socketio').default(socketio);
require('./config/express').default(app);
require('./routes').default(app);

// Start server
function startServer() {
  app.shpeelyServer = server.listen(config.port, config.ip, () => {
    logger.info('Express server listening on %d, in %s mode', config.port, app.get('env'));
  });
}

// Start crons
tasks.start();

setImmediate(startServer);

// Expose app
exports = module.exports = app;
