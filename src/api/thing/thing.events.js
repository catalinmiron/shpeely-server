/**
 * Thing model events
 */


import { EventEmitter } from 'events';
import Thing from './thing.model';

const ThingEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
ThingEvents.setMaxListeners(0);

// Model events
const events = {
  save: 'save',
  remove: 'remove',
};

function emitEvent(event) {
  return function (doc) {
    ThingEvents.emit(`${event}:${doc._id}`, doc);
    ThingEvents.emit(event, doc);
  };
}

// Register the event emitter to the model events
Object.keys(events).forEach(e => Thing.schema.post(e, emitEvent(events[e])));

export default ThingEvents;
