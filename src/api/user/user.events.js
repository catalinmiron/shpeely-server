/**
 * User model events
 */


import { EventEmitter } from 'events';
import User from './user.model';

const UserEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
UserEvents.setMaxListeners(0);

// Model events
const events = {
  save: 'save',
  remove: 'remove',
};

function emitEvent(event) {
  return function (doc) {
    UserEvents.emit(`${event}:${doc._id}`, doc);
    UserEvents.emit(event, doc);
  };
}

// Register the event emitter to the model events
Object.keys(events).forEach(e => User.schema.post(e, emitEvent(events[e])));

export default UserEvents;
