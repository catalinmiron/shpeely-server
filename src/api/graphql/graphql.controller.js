/**
 * Using Rails-like standard naming convention for endpoints.
 * POST    /api/statistics              ->  create
 */

import fetch from 'node-fetch';
import logger from 'winston';
import config from '../../config/environment';
import {syncPlain as getBggData} from '../bgg/bgg.controller';
import { emitGameResultUpdated, emitGameResultDeleted } from './graphql.events';
import { deleteGameresult } from '../../services/graphql/gameresults';

function handleError(res, err) {
  if (config.env !== 'production') {
    console.error(err);
    res.status(500).send();
  }
}

function forwardResponse(res) {
  return function (remoteResponse) {
    return res.status(remoteResponse.status).json(remoteResponse.json());
  };
}

async function gameresultHandler (req, res) {
  // transform body to game-stats conform object
  const gameresult = req.body.data.Gameresult.node;

  if (gameresult.deleted) {
    // game result was deleted -> Remove related scores and update game stats
    logger.debug(`Game result ${gameresult.id} was deleted.`);

    try {
      await deleteGameresult(gameresult.id);
    } catch (err) {
      logger.warn('Failed to delete gameresult: ', err);
    }

    await fetch(`${config.gameStats.uri}/tournament/${gameresult.tournament.id}/gameresult/${gameresult.id}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json'
      }}).then(forwardResponse(res));

    return emitGameResultDeleted(gameresult.tournament.id, gameresult.id);

  } else {
    // game result was modified -> update game stats
    const tournamentId = gameresult.tournament.id;

    const gameStatsRequestBody = {
      id: gameresult.id,
      time: gameresult.time,
      bggid: gameresult.bggInfo.bggid.toString(),
      scores: gameresult.scores.map(s => ({ player: s.player.id, score: s.score }))
    };

    logger.debug('Posting to game statistics: ', gameStatsRequestBody);

    // load BGG data
    const { weight, bggid } = await getBggData(gameStatsRequestBody.bggid);

    // Post weight to game stats
    await fetch(`${config.gameStats.uri}/game/${bggid}/weight/${weight}`, {
      method: 'POST'
    });

    // Post result to game stats
    await fetch(`${config.gameStats.uri}/tournament/${tournamentId}/gameresult`, {
        method: 'POST',
        body: JSON.stringify(gameStatsRequestBody),
        headers: {
          'Content-Type': 'application/json'
        }})
      .then(forwardResponse(res));

    emitGameResultUpdated(tournamentId, gameresult.id);
  }
}

const handlers = {
  Gameresult: gameresultHandler
}

/**
 * Single entrypoint for all calls from GraphCool
 * @param req
 * @param res
 */
export function handleRequest(req, res) {
  try {
    const topic = Object.keys(req.body.data)[0];

    logger.debug('Got request from GraphQL: ',  topic);

    const handler = handlers[topic];
    if (!handler) {
      logger.warn('No handler found for topic ', topic);
      return res.status(404).json({err: `No handler registered for topic ${topic}`});
    }

    return handler(req, res)
      .catch(err => handleError(res, err));
  } catch (err) {
    handleError(res, err);
  }
}
