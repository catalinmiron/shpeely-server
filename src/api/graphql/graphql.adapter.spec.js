import newGameResult from './fixtures/newGameResult4.json';

import {transformGameResult} from './graphql.adapter';

describe('GraphQL Adapter:', () => {

  describe('Create Game Result', () => {

    it('Should transform the data to the correct format', () => {
      const result = transformGameResult(newGameResult);
      expect(result.id).to.equal('gameresult-id');
      expect(result.bggid).to.equal('31260');
      expect(result.scores).to.have.deep.members([
        { player: 'player1-id', score: 10 },
        { player: 'player2-id', score: 20 },
      ]);
    });
  });

});
