

const proxyquire = require('proxyquire').noPreserveCache();

const statisticsCtrlStub = {
  handleRequest: 'statisticsCtrl.handleRequest',
};

const routerStub = {
  post: sinon.spy()
};

// require the index with our stubbed out modules
const statisticsIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    },
  },
  './graphql.controller': statisticsCtrlStub,
});

describe('statistics API Router:', () => {
  it('should return an express router instance', () => {
    expect(statisticsIndex).to.equal(routerStub);
  });

  describe('POST /api/statistics', () => {
    it('should route to statistics.controller.handleRequest', () => {
      expect(routerStub.post
        .withArgs('/', 'statisticsCtrl.handleRequest'),
        ).to.have.been.calledOnce;
    });
  });

});
