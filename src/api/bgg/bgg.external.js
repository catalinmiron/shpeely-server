import logger from 'winston';
import config from '../../config/environment';
import _ from 'lodash';
import fetch from 'node-fetch';
import striptags from 'striptags';
import he from 'he';
import cheerio from 'cheerio';
import vm from 'vm';

// CONSTANTS

// api
const BGG_API = config.bggMirror || 'https://boardgamegeek.com';

logger.debug('Using BGG Api at ', BGG_API);

// thumbnails
const THUMBNAIL_BASE_URL = 'https://cf.geekdo-images.com/images/pic';
const THUMBNAIL_EXT = 'jpg';
const THUMBNAIL_SIZE = {
  small: 't', // 150x150
  large: 'md', // 500x500
};


function getImageUrl(imageId, size) {
  if (!imageId) {
    return null;
  }
  return `${THUMBNAIL_BASE_URL}${imageId}_${size}.${THUMBNAIL_EXT}`;
}

export function createModel(bggInfo, bggStats) {
  let model;

  const bggid = bggInfo.item.objectid;

  /**
   * special case for weight. Weight is a required field but not all games have a weight defined. For lack of a better
   * soltion, I'm setting the weight to 1 for now. In the future maybe it makes sense to filter such games out from
   * the beginning, or letting the user define a weight, or finding the weight of the base game if the game is an
   * expansion... Or some other solution...
   */
  let weight = _.get(bggStats, 'item.polls.boardgameweight.averageweight');

  if (!weight) {
    logger.debug('Game with id %s has no weight. Setting weight of 1', bggid);
    weight = 1;
  }

  try {
    if (!bggStats.item.rankinfo) {
      console.error(bggInfo)
      console.error(bggStats)
    }
    const overallRankInfo = bggStats.item.rankinfo.find(x => x.subdomain === null)

    model = {
      bggid,
      weight,
      name: bggInfo.item.name,
      lastUpdate: Date.now(),
      maxPlayers: Number(bggInfo.item.maxplayers),
      thumbnail: bggInfo.item.images.thumb,
      description: he.decode(striptags(bggInfo.item.description)),
      rating: Number(overallRankInfo.baverage),
      rank: Number(overallRankInfo.rank),
      ownedBy: Number(bggStats.item.stats.numowned),
      playTime: Number(bggInfo.item.maxplaytime),
      yearPublished: Number(bggInfo.item.yearpublished)
    };

    return model;

  } catch (err) {
    logger.error('Failed to create model from BGG data.', err);
    logger.error(
      'Incopatible data received from BGG:\n %s',
      JSON.stringify(bggInfo, null, 2),
      JSON.stringify(bggStats, null, 2)
    );
    throw new Error('Failed to parse data from BGG API');
  }
}

function fetchStats(id) {
  // Unfortunately the 'dynamicinfo' endpoint does not seem to be accessible anymore from the outside.
  // The data is already loaded with the HTML and assigned to a global GEEK variable. So in order to get to the data,
  // I need to load the HTML page, evaluate the scripts in a sandboxed vm and can then access the global GEEK variable.

  const globals = {};
  const context = vm.createContext(globals);

  return fetch(`${BGG_API}/boardgame/${id}`)
    .then(res => res.text())
    .then(cheerio.load)
    .then($ => {
      // iterate over all scripts and evaluate them in a sandbox
      $('script').each((i, script) => {
        try {
          const scriptContent = script.children[0].data;
          const sandbox = new vm.Script(scriptContent);
          sandbox.runInContext(context);
        } catch (err) {
          // ignore
        }
      });

      if (!globals.GEEK.geekitemPreload) {
        logger.error(`Failed to load stats for game ${id} from the BGG site.`);
        return null;
      }

      return globals.GEEK.geekitemPreload;
    })
}

function fetchInfo(id) {
  return fetch(`${BGG_API}/api/geekitems?objecttype=thing&objectid=${id}`)
    .then(res => res.json())
}

function mapSearchResults(item) {
  return {
    id: Number(item.objectid),
    year: item.yearpublished,
    name: item.name,
    imageSmall: getImageUrl(item.rep_imageid, THUMBNAIL_SIZE.small),
    imageLarge: getImageUrl(item.rep_imageid, THUMBNAIL_SIZE.large),
  };
}

export function searchBgg(query, maxResults) {
  const config = {
    headers: {Accept: 'application/json'}
  };
  return fetch(`${BGG_API}/search/boardgame?q=${query}&showcount=${maxResults || 20}`, config)
    .then(res => res.json())
    .then(res => res.items.map(mapSearchResults))
}

export function getModelFromBgg(id) {
  return Promise.all([fetchInfo(id), fetchStats(id)])
    .then(([info, stats]) => createModel(info, stats))
}
