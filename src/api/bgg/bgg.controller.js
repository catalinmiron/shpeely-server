/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/bggs              ->  index
 * POST    /api/bggs              ->  create
 * GET     /api/bggs/:id          ->  show
 * PUT     /api/bggs/:id          ->  upsert
 * DELETE  /api/bggs/:id          ->  destroy
 */

import logger from 'winston';
import mongoose from 'mongoose';
import BggData from './bgg.model';
import { getModelFromBgg, searchBgg } from './bgg.external';
import { upsertBggInfo } from '../../services/graphql';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    console.error(err);
    logger.error(err);
    res.status(statusCode).send(err);
    return null;
  };
}

function isObjectId(id) {
  return mongoose.Types.ObjectId.isValid(id.toString())
}

// Loads a document from the DB by objectId or bgg id
export function getById(id) {
  if (isObjectId(id)) {
    // mongo id
    logger.debug('Looking up game with mongo id %s', id);
    return BggData.findById(id).exec()
  }

  // BGG id
  logger.debug('Looking up game with bgg id %s', id);
  return BggData.findOne({ bggid: id });
}

// Gets a list of BggDatas
export function index(_, res) {
  return BggData.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a list of BggDatas matching the query without express response
export function queryPlain(query) {
  return BggData.find(query).exec();
}

// Gets a list of BggDatas matching the query
export function query(req, res) {
  return queryPlain(req.query)
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// search a game on BGG
export function search(req, res) {
  return searchBgg(req.query.query, req.query.maxResults)
    .then((items) => res.json(items))
    .catch(handleError(res))
}

// Get the oldest document
export function getOldest() {
  return BggData.findOne().sort({ updatedAt: 1 });
}

// Creates a new BggData in the DB without a request
export function createPlain(doc) {
  return BggData.create(doc);
}

// Gets a single BggData from the DB. The id can either be a mongo id or the BGG id.
// If a BGG id was provided that is not in the DB, the data will be loaded from BGG and
// saved in the DB.
export function show(req, res) {
  return getById(req.params.id)
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new BggData in the DB
export function create(req, res) {
  return createPlain(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}


export function upsertPlain(id, body) {
  const query = isObjectId(id) ? { _id: id } : { bggid: id };
  return BggData.findOneAndUpdate(query, body, {
    new: true,
    upsert: true,
    setDefaultsOnInsert: true,
    runValidators: true,
  }).exec();
}


// Upserts the given BggData in the DB at the specified ID
export function upsert(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return upsertPlain(req.params.id, req.body)
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Synchronizes with BGG (updates data or creates a new record in the DB)
export function syncPlain(bggid) {
  return getModelFromBgg(bggid)
    .then((model) => Promise.all([model, upsertBggInfo(model)]))
    .then(([model, { id }]) => ({ id, ...model }))
    .then(model => upsertPlain(bggid, model));
}

// Synchronizes with BGG (updates data or creates a new record in the DB)
export function sync(req, res) {
  return getById(req.params.id)
    .then((data) => {
      if (data) { return data.bggid; }
      return req.params.id;
    })
    .then(syncPlain)
    .then(respondWithResult(res))
    .catch(handleError(res));
}


// Deletes a BggData from the DB
export function destroy(req, res) {
  return getById(req.params.id)
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}

// Deletes all BGG data from the DB
export function destroyAllPlain() {
  return BggData.remove({}).exec();
}
