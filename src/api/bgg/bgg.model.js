import mongoose from 'mongoose';

const BggDataSchema = new mongoose.Schema({
  id: { type: String, optional: false, unique: true },
  bggid: { type: Number, optional: false, unique: true },
  weight: { type: Number, optional: false },
  maxPlayers: { type: Number, optional: false },
  name: { type: String, optional: false },
  thumbnail: String,
  description: String,
  rating: Number,
  rank: Number,
  ownedBy: Number,
  playTime: Number,
  yearPublished: Number
}, {
  timestamps: true
});

export default mongoose.model('BggData', BggDataSchema);
