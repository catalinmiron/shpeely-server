

// Set default node environment to development
const env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';

if (env === 'development' || env === 'test' || process.env.BABEL_RUNTIME) {
  // Register the Babel require hook

  // eslint-disable-next-line import/no-extraneous-dependencies, global-require
  require('babel-register');
}

// Export the application
exports = module.exports = require('./app');
