import graphQLClient from './index'

const createBggInfoMutation = `
  mutation (
    $bggid: Int!
    $description: String!
    $maxPlayers: Int!
    $name: String!
    $playTime: Int!
    $rank: Int!
    $rating: Float!
    $thumbnail: String!
    $yearPublished: Int!
    $ownedBy: Int!
    $weight: Float!
  ) {
    bggInfo: createBggInfo (
      bggid: $bggid,
      description: $description,
      maxPlayers: $maxPlayers,
      name: $name,
      playTime: $playTime,
      rank: $rank,
      rating: $rating
      thumbnail: $thumbnail,
      yearPublished: $yearPublished,
      ownedBy: $ownedBy
      weight: $weight
    ) {
      id
    }
  }
`;

const updateBggInfoMutation = `
  mutation (
    $id: ID!
    $bggid: Int
    $description: String
    $maxPlayers: Int
    $name: String
    $playTime: Int
    $rank: Int
    $rating: Float
    $thumbnail: String
    $yearPublished: Int
    $ownedBy: Int
    $weight: Float
  ) {
    bggInfo: updateBggInfo (
      id: $id,
      bggid: $bggid,
      description: $description,
      maxPlayers: $maxPlayers,
      name: $name,
      playTime: $playTime,
      rank: $rank,
      rating: $rating
      thumbnail: $thumbnail,
      yearPublished: $yearPublished,
      ownedBy: $ownedBy
      weight: $weight
    ) {
      id
    }
  }
`;

const getBggInfoByBggIdQuery = `
  query ($bggid: Int!) {
    bggInfo: BggInfo (
       bggid: $bggid
    ) {
      id
    }
  }
`;


export function upsertBggInfo (bggInfoModel) {
  return getBggInfoByBggId(bggInfoModel.bggid)
    .then((bggInfo) => {
      if (!bggInfo) {
        return createBggInfo(bggInfoModel)
      }
      return updateBggInfo(bggInfo.id, bggInfoModel)
    })
}

export function getBggInfoByBggId (bggid) {
  return graphQLClient.request(getBggInfoByBggIdQuery, { bggid })
    .then(({ bggInfo }) => bggInfo)
}

export function updateBggInfo (id, bggInfo) {
  return graphQLClient.request(updateBggInfoMutation, { id, ...bggInfo })
    .then(({ bggInfo }) => bggInfo)
}

export function createBggInfo (bggInfo) {
  return graphQLClient.request(createBggInfoMutation, bggInfo)
    .then(({ bggInfo }) => bggInfo)
}
