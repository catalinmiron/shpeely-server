import app from './src';
import mongoose from 'mongoose';

after(function(done) {
  app.shpeelyServer.on('close', () => done());
  mongoose.connection.close();
  app.shpeelyServer.close();
});
