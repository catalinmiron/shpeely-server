FROM node:8.1.2
MAINTAINER Shpeely <info@shpeely.com>

ENV PORT 80
ENV SECURE_PORT 443

EXPOSE 80
EXPOSE 443

RUN npm install -g yarn

ADD . /app
WORKDIR /app
RUN npm install && npm install -g gulp
RUN gulp build

# Needs to be set after building, otherwise the
# dev dependnecies are not installed (eg. gulp is missing)
ENV NODE_ENV production

CMD ["node", "dist/app.js"]
